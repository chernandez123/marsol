/* Variables globales */

const arrayCodigosItems = [];
const arrayDescripcionItems = [];

//**********************************

function estaPermitido(nombreUsuario, usuariosPermitidos, password)
{
	for (var i=0; i<usuariosPermitidos.length; i++)
	{
		if (usuariosPermitidos[i] == nombreUsuario)
		{
			if (password == "EstaEsMiSuperClave")
			{
				return true;
			}
			return false;
		}
	}
	return false;
}

/*
	Retorna array con fechas del historial de ventas por día en formato dd/mm/YYYY.
*/
function obtenerFechasHistorialVentasPorDia(json)
{
	let arrayFechas = [];

	for (var i=0; i<json.length; i++)
	{
		let valoresSplit = json[i].fecha.split("T");
		arrayFechas.push(valoresSplit[0]);
	}
	return arrayFechas;
}

function obtenerCantidadVendidaHistorialVentasPorDia(json)
{
	let arrayCantidadVendidas = [];

	for (var i=0; i<json.length; i++)
	{
		arrayCantidadVendidas.push(json[i].cantidad_vendida);
	}
	return arrayCantidadVendidas;
}

function obtenerTotalVendidoHistorialVentasPorDia(json)
{
	let arrayTotalVendido = [];

	for (var i=0; i<json.length; i++)
	{
		arrayTotalVendido.push(json[i].total_vendido);
	}
	return arrayTotalVendido;
}

function obtenerArrayRGBA(lengthArray)
{
	let array = [];

	for (var i=0; i<lengthArray; i++)
	{
		if (i%2==0)
		{
			array.push('rgba(255, 99, 132, 0.2)');
		}
		else
		{
			array.push('rgba(54, 162, 235, 0.2)');
		}
	}
	return array;
}

function obtenerArrayRGBABorder(lengthArray)
{
	let array = [];

	for (var i=0; i<lengthArray; i++)
	{
		if (i%2==0)
		{
			array.push('rgba(255, 99, 132, 1)');
		}
		else
		{
			array.push('rgba(54, 162, 235, 1)');
		}
	}
	return array;
}

function obtenerTagsMaquinasDisponibles(json)
{
	let arrayTagsMaquinas = [];

	for (var i=0; i<json.length; i++)
	{
		arrayTagsMaquinas.push(json[i].descripcion);
	}
	return arrayTagsMaquinas;
}

function obtenerCodigosItemsSeleccionados(itemsSeleccionados, arrayDescripcionItems, arrayCodigosItems)
{
	const codigosSeleccionados = [];

	for (var i=0; i<itemsSeleccionados.length; i++)
	{
		for (var j=0; j<arrayDescripcionItems.length; j++)
		{
			if (itemsSeleccionados[i] == arrayDescripcionItems[j])
			{
				codigosSeleccionados.push(arrayCodigosItems[j]);
			}
		}
	}
	return codigosSeleccionados;	
}

function graficarItemsSeleccionados(codigosItemsSeleccionados, itemsSeleccionados)
{
	$("#rowItems").empty();

	for (let i=0; i<codigosItemsSeleccionados.length; i++)
	{
		let historialVentaPorDiaPorProducto = 'https://marsol-test.herokuapp.com/unsecure/history/item/'+codigosItemsSeleccionados[i];

		$.getJSON(historialVentaPorDiaPorProducto, function(data) {

		    //**** Obtener arreglo de fechas ****
		    let arrayFechasHistorialVentasPorDia = obtenerFechasHistorialVentasPorDia(data);
		    let arrayCantidadVendidas = obtenerCantidadVendidaHistorialVentasPorDia(data);
		    let arrayTotalVendido = obtenerTotalVendidoHistorialVentasPorDia(data);
		    const arrayRGBA = obtenerArrayRGBA(arrayTotalVendido.length);
		    const arrayRGBABorder = obtenerArrayRGBABorder(arrayTotalVendido.length);

		    $("#rowItems").append(
		    	'<div class="col-md-12 panelGrafico">'
				+	'<div class="col-md-7 col-md-offset-1">'
				+		'<div class="panel panel-primary">'
				+			'<div class="panel-heading"><strong>Historial de Ventas por Día para '+itemsSeleccionados[i]+'</strong></div>'
				+			'<div class="panel-body">'
				+				'<canvas id="chartHistorialVentasPorDiaPorProducto_'+codigosItemsSeleccionados[i]+'" width="400" height="400"></canvas>'
				+			'</div>'
				+		'</div>'
				+	'</div>'
				+	'<div class="col-md-4">'
				+		'<div class="panel panel-primary">'
				+			'<div class="panel-heading">Información adicional</div>'
				+			'<div class="panel-body">'
				+				'<h5><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> <strong>Mayor índice de ventas: </strong><p id="mayorIndiceVentas"> (fecha) - valor</p></h5>'
				+				'<h5><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> <strong>Menor índice de ventas: </strong><p id="menorIndiceVentas"> (fecha) - valor</p></h5>'
				+				'<h5><strong>Promedio índice de ventas: </strong><p id="promedioIndiceVentas"> (fecha) - valor</p></h5>'
				+			'</div>'
				+		'</div>'
				+	'</div>'
				+'</div>'
				+'<hr>'
		    );

		    var ctx = $("#chartHistorialVentasPorDiaPorProducto_"+codigosItemsSeleccionados[i]);
			/*var myChart = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: arrayFechasHistorialVentasPorDia,
			        datasets: [{
			            label: 'Unidades vendidas según fecha',
			            data: arrayCantidadVendidas,
			            backgroundColor: arrayRGBA,
			            borderColor: arrayRGBABorder,
			            borderWidth: 1
			        }]
			    },
			    options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero: true
			                }
			            }]
			        }
			    }
			});*/

			var mixedChart = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        datasets: [{
			            label: 'Unidades vendidas según fecha',
			            data: arrayCantidadVendidas,
			            backgroundColor: arrayRGBA,
			            borderColor: arrayRGBABorder,
			            yAxisID: 'first-y-axis',
			            borderWidth: 1
			        }, {
			            label: 'Dinero obtenido de las ventas por fecha',
			            data: arrayTotalVendido,
			            backgroundColor: '#050505',
			            yAxisID: 'second-y-axis',
			            type: 'line',
			            fill: false
			        }],
			        labels: arrayFechasHistorialVentasPorDia
			    },
			    options: {
			        scales: {
			            yAxes: [{
		                    id: 'first-y-axis',
		                    type: 'linear'
		                }, {
		                    id: 'second-y-axis',
		                    type: 'linear'
		                }]
			        }
			    }
			});
		});
	}
}

$(document).ready(function(){

	const usuariosPermitidos = ["abustos","ndiaz", "mrodriguez","emarin"];
	const password = "EstaEsMiSuperClave";

	$("#buttonIniciarSesion").click(function()
	{
		let nombreUsuario = $("#inputTextUsuario").val().toString();
		let password = $("#inputTextPassword").val().toString();

		if (estaPermitido(nombreUsuario, usuariosPermitidos, password))
		{
			$("#rowGeneral").empty(); //Vaciar contenedor general para proceder a mostrar resultados

			/******************************************/

			$("#rowGeneral").append(
				'<div class="col-md-7 col-md-offset-1">'
			+		'<div class="col-md-12">'
			+			'<div class="row">'
			+				'<div class="col-md-6">'
			+					'<h5>Items Disponibles</h5>'
			+					'<select class="form-control" multiple="multiple" id="selectItems">'
			+					'</select>'
			+				'</div>'
			+			'</div>'
			+		'</div>'
			+	'</div>'
			);

			/* Obtener tags de máquinas disponibles  */

			const jsonItemsDisponibles = 'https://marsol-test.herokuapp.com/unsecure/items'; 

			/* Inicializar select de máquinas obtenidas de json de items */

			$.getJSON(jsonItemsDisponibles, function(data) {

				/* Obtener códigos y descrpción de máquinas (items) disponibles */

				for (var i=0; i<data.length; i++)
				{
					arrayCodigosItems.push(data[i].codigo);
					arrayDescripcionItems.push(data[i].descripcion);
				}

				$('#selectItems').select2({
	                placeholder: 'Selecciona algún ítem',
	                tags: arrayDescripcionItems
	            }); 
			});

			/* Agregar gráfico de historial de ventas por fecha (historial general) */

			$("#rowGeneral").append(
				'<div class="col-md-12 panelGrafico">'
				+	'<div class="col-md-7 col-md-offset-1">'
				+		'<div class="panel panel-primary">'
				+			'<div class="panel-heading"><strong>Historial de Ventas por Día</strong></div>'
				+			'<div class="panel-body">'
				+				'<canvas id="chartHistorialVentasPorDia" width="400" height="400"></canvas>'
				+			'</div>'
				+		'</div>'
				+	'</div>'
				+	'<div class="col-md-4">'
				+		'<div class="panel panel-primary">'
				+			'<div class="panel-heading">Información adicional</div>'
				+			'<div class="panel-body">'
				+				'<h5><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> <strong>Mayor índice de ventas: </strong><p id="mayorIndiceVentas"> (fecha) - valor</p></h5>'
				+				'<h5><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> <strong>Menor índice de ventas: </strong><p id="menorIndiceVentas"> (fecha) - valor</p></h5>'
				+				'<h5><strong>Promedio índice de ventas: </strong><p id="promedioIndiceVentas"> (fecha) - valor</p></h5>'
				+			'</div>'
				+		'</div>'
				+	'</div>'
				+'</div>'
				+'<hr>'
			);

			//**********************************************************
			//****** Leer JSON de historial de ventas por día **********

			const historialVentasPorDia = 'https://marsol-test.herokuapp.com/unsecure/history'; 

			$.getJSON(historialVentasPorDia, function(data) {

			    //**** Obtener arreglo de fechas ****
			    let arrayFechasHistorialVentasPorDia = obtenerFechasHistorialVentasPorDia(data);
			    let arrayCantidadVendidas = obtenerCantidadVendidaHistorialVentasPorDia(data);
			    let arrayTotalVendido = obtenerTotalVendidoHistorialVentasPorDia(data);
			    const arrayRGBA = obtenerArrayRGBA(arrayTotalVendido.length);
			    const arrayRGBABorder = obtenerArrayRGBABorder(arrayTotalVendido.length);

			    var ctx = $("#chartHistorialVentasPorDia");
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: {
				        labels: arrayFechasHistorialVentasPorDia,
				        datasets: [{
				            label: 'Unidades vendidas según fecha',
				            data: arrayCantidadVendidas,
				            backgroundColor: arrayRGBA,
				            borderColor: arrayRGBABorder,
				            borderWidth: 1
				        }]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				                    beginAtZero: true
				                }
				            }]
				        }
				    }
				});
			});

			//**********************************************************
			//****** Venta por producto ********************************

			/* Si se agrega o quita una máquina del select, se actualizan los gráficos  */

			$("#selectItems").on("change", function (e) 
		    { 
		        let itemsSeleccionados = $("#selectItems").val().toString().split(",");
		        //Dado los items seleccionados, busco sus códigos (id's)

		        let codigosItemsSeleccionados = obtenerCodigosItemsSeleccionados(itemsSeleccionados, arrayDescripcionItems, arrayCodigosItems);
		    	console.log(codigosItemsSeleccionados);
		    	graficarItemsSeleccionados(codigosItemsSeleccionados, itemsSeleccionados);
		    });

		}
		else
		{
			alert("Combinacion de usuario y contraseña no encontrado");
		}
	});

	

})

